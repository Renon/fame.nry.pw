---
title: le groupe
date: 2023-12-11T21:34:14.604Z
draft: false
---
La FAME est également un groupe de musiciens, né en 2016 durant le mouvement des Nuits Debouts, dans l'idée d'exploiter la musique comme force de soutien, voire de proposition, dans un cadre activiste et/ou militant. Nous combattons leur argent avec nos cuivres. L'adhésion au groupe est entièrement ouverte. Aucune expertise musicale n'est évidemment requise.

## **Répétition (presque) chaque mardi dès 19h au [Chat Perché](https://www.chatperche.org/ "https\://www.chatperche.org/") ¹**

¹ Dont l'équipe nous prête plus qu'aimablement les locaux (auto-chauffés par chaleur humaine). Qu'elle en soit ici grâssement remerciée.