---
title: "les actions"
date: 2018-11-15T14:02:29+01:00
draft: false
---

La FAME s'est construite sur de nombreuses participations à des manifestations de tous genres, à l'initiative de ses membres, voire sur invitations expresses, sans autre concertation préalable que quelques réponses par mail. Nous avons à coeur néanmoins de faire évoluer cette pratique, non seulement pour diversifier les causes et les modes d'actions, mais aussi pour enrichir nos positions respectives au travers d'échanges approfondis en répét' ou débrief' post-actions. La Fame suit à sa façon et dans une organisation beaucoup plus douteuse, le sillon tracé par la [Fanfare Invisible](http://lafanfareinvisible.fr/accueil.html), fanfare de lutte basée à Paris.

Les luttes que nous soutenons se partagent principalement entre :

- L'aide aux migrants
- L'anti capitalisme face la casse du service publique
- L'anti capitalisme face à la destruction des écosystèmes
- Le soutien aux personnes à la rue
- Les actions citoyennes contre la finance
- L'usage de la bicyclette, sortie de l'automobile
- Féminismes en actes
- Droits LGBTQA+
- Sensibilisation et Résistance aux GAFAM
