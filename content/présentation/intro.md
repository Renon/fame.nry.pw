---
title: "le virus"
date: 2018-11-15T14:19:19+01:00
draft: false
---
La FAME est un virus théorique. FAME signifie l'abandon de soi aux évènements. Dites oui à une vie tendant résolument à sa propre négation.  FAME se refuse à être contemporain de toute cette merde. FAME réconcilie l'art et le quotidien afin qu'ils puissent avoir des rapports sexuels contre-nature dans des ruelles mal FAMées. FAME, c'est l'apatridie de l'esprit, l'ennemi de la masse et du parti. FAME vomit autant l'ordre stylé que le désordre stylisé propres à l'esthétique contemporaine.
