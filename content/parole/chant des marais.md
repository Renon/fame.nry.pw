---
title: "Chant des marais"
date: 2019-01-16T15:54:07+01:00
draft: false
---

*I*  
Loin vers l'infini s'étendent  
De grands prés marécageux  
Et là-bas nul oiseau ne chante  
Sur les arbres secs et creux  
  
*Refrain*  
Ô terre de détresse  
Où nous devons sans cesse  
Piocher, piocher.  
  
*II*
Dans ce camp morne et sauvage  
Entouré de murs de fer  
Il nous semble vivre en cage  
Au milieu d'un grand désert.  
  
*III*  
Bruit des pas et bruit des armes  
Sentinelles jours et nuits  
Et du sang, et des cris, des larmes  
La mort pour celui qui fuit.  
  
*IV*  
Mais un jour dans notre vie  
Le printemps refleurira.  
Liberté, liberté chérie  
Je dirai : « Tu es à moi. »  
  
*Dernier refrain*  
Ô terre enfin libre  
Où nous pourrons revivre,  
Aimer, aimer.  
  
*Dernier refrain*  
Ô terre d’allégresse  
Où nous pourrons sans cesse  
Aimer, aimer  
 
