---
title: "Ma chanson du mirliton"
date: 2018-11-14T20:54:07+01:00
draft: true
---

Exemple de texte de chanson.

Pensez à modifier le champ title et date ci-dessus, et à remplacer "draft: true" par "draft: false" lorsque le texte est prêt à être publié.

la balise <br \> indique le retour à la ligne, elle est indispensable pour ne pas avoir un caca difforme en sortie.

Puis mon grand-père s'en est allé <br \>
Un vent mauvais l'a emporté <br \>
Et je reste seul sous le porche <br \>
En regardant jouer d'autres gosses <br \>
Dansant autour du vieux pieu noir <br \>
Où tant de mains se sont usées <br \>
Je chante des chansons d'espoir <br \>
Qui parlent de la liberté <br \>
<br \>
Mais si nous...<br \>

