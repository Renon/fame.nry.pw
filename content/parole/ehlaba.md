---
title: "Hé là-bas (Ôde aux casseur·euses)"
date: 2020-01-21T20:54:07+01:00
draft: false
---

Paroles des Canulars (Lyon), 2019

En mille sept cent quatre-vingt neuf,  
Des gueux ont attaqué les keufs,  
À coups de fourches et de bâtons,     
Ils ont libéré la prison,   
  
Et tous les 14 juillet,   
Quand t'applaudis le défilé,   
T'oublies de dire, j'me demande  pourquoi,   
Qu'ils ont coupé la tête au roi   
  
Refrain :   
Non non non x2,   
C’est pas bien d’ casser x2   
Sauf quand on x2,   
Quand on a gagné x2   
  
Pendant la guerre les maquisards,   
Faisaient sauter les trains les gares,   
Aujourd'hui tu leur rends hommage,   
Toujours au passé c'est dommage,   
  
Et quand aux monuments aux morts,   
Tu les véneres tu les honores,   
T'oublies de dire que les fascistes,   
Les traitaient de terroristes,   
  
Refrain   
  
Mille neuf cent trois, les meufs anglaises,   
Avaient osé c'est balèze,   
Casser les vitres des entreprises,   
Et foutre le feu aux églises,   
  
Et quand pour les présidentielles,   
Tu loues l'suffrage universel,   
T'oublies de dire c'est pas normal,   
Qu'c'est grâce à ça si c'est légal,   
  
Refrain   
  
Quand dans les manifestations,   
On dépave les illusions,   
Et qu'on balance des utopies,   
À la gueule de la bourgeoisie,   
  
En été quand tu vas bronzer,   
Quand tes médocs sont remboursés,   
T'oublies que grâce à cette violence,   
T'as la sécu et tes vacances,   
  
Refrain   
  
Non non non   
C'est pas bien d'casser   
Et on va   
Et on va gagner   




