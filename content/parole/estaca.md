---
title: "Estaca"
date: 2018-11-14T20:54:07+01:00
draft: false
---

 Du temps où je n'étais qu'un gosse   
Mon grand-père me disait souvent   
Assis à l'ombre de son porche   
En regardant passer le vent   
Petit vois-tu ce pieu de bois   
Auquel nous sommes tous enchaînés   
Tant qu'il sera planté comme ça   
Nous n'aurons pas la liberté   
  
Mais si nous tirons tous, il tombera   
Ca ne peut pas durer comme ça   
Il faut qu'il tombe, tombe, tombe   
Vois-tu comme il penche déjà   
Si je tire fort il doit bouger   
Et si tu tires à mes côtés   
C'est sûr qu'il tombe, tombe, tombe   
Et nous aurons la liberté   
  
Petit ça fait déjà longtemps   
Que je m'y écorche les mains   
Et je me dis de temps en temps   
Que je me suis battu pour rien   
Il est toujours si grand si lourd   
La force vient à me manquer   
Je me demande si un jour   
Nous aurons bien la liberté   
  
Mais si nous...   
  
Puis mon grand-père s'en est allé   
Un vent mauvais l'a emporté   
Et je reste seul sous le porche   
En regardant jouer d'autres gosses   
Dansant autour du vieux pieu noir   
Où tant de mains se sont usées   
Je chante des chansons d'espoir   
Qui parlent de la liberté   
  
Mais si nous...  

