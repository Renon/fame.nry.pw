---
title: "Hymne des femmes"
date: 2019-01-14T20:54:07+01:00
draft: false
---

Nous, qui sommes sans passé, les femmes,  
Nous qui n'avons pas d'histoire,  
Depuis la nuit des temps, les femmes,  
Nous sommes le continent noir.  
  
*Refrain :*  
Debout femmes esclaves  
Et brisons nos entraves  
Debout ! debout !  
  
Asservies, humiliées, les femmes,  
Achetées, vendues, violées,  
Dans toutes les maisons, les femmes,  
Hors du monde reléguées.  
  
*Refrain*  
  
Seule dans notre malheur, les femmes,  
L'une de l'autre ignorée,  
Ils nous ont divisées, les femmes,  
Et de nos sœurs séparées.  
  
*Refrain*  
  
Reconnaissons-nous, les femmes,  
Parlons-nous, regardons-nous,  
Ensemble on nous opprime, les femmes,  
Ensemble révoltons-nous.  
  
*Refrain*  
  
Le temps de la colère, les femmes,  
Notre temps est arrivé,  
Connaissons notre force, les femmes,  
Découvrons-nous des milliers.  
  
*Dernier refrain :*  
Levons-nous femmes esclaves  
Et jouissons sans entrave  
Debout, debout, debout !  
 
