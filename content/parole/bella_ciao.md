---
title: "Avançons à l'unisson"
date: 2018-11-14T20:54:07+01:00
draft: false
---

Inébranlables,  
Infatigables  
Avançons, avançons, avançons  
Du monde en descente  
Remontons la pente  
Avançons à l'unisson  
  
 Sur nous ils spéculent  
Ils rêvent qu'on recule  
Avançons, avançons, avançons  
Ils pensent qu'à leur gueule  
Ils se croient tous seuls  
Avançons à l'unisson  
  
 Tout le mond’ se couche  
Dès qu'ils ouvr' la bouche  
Avançons, avançons, avançons  
La justice détale  
Et l'Etat s'étale  
Avançons à l'unisson  
  
 Veulent nous encager  
Pour tout diriger  
Avançons, avançons, avançons  
Ils dirigent c'est sûr  
Tout droit dans le mur   
Avançons à l'unisson  
  
 Pour pas s'écraser   
On va tout freiner  
Avançons, avançons, avançons  
Et en s'écoutant  
Reprendre le temps  
Avançons à l'unisson  
  
 Nous on ne sait pas  
Vraiment où on va  
Avançons, avançons, avançons  
Mais main dans la main  
C'est un autr' chemin  
Avançons à l'unisson  
  
 Mais main dans la main  
C'est un notr' chemin   
Avançons à l'unisson (ralentissant)  
