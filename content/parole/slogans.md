---
title: "Slogans RESF2017"
date: 2018-12-14T16:49:07+01:00
draft: false
---

Proposition de slogans de soutien aux réfugiéEs   


Non, non, non, à la loi Macron   
liberté de circulation, et d’installation   

Un toit, une école, des papiers pour tous   

So, so, so, solidarité   
Avec les réfugiéEs, du monde entier !   

Un logement pour tous ! La solution : réquisition   

De l’air, de l’air : ouvrez les frontières !   

ABROGATION de toutes les lois racistes   
Solidarité internationaliste   

C'est pas les sans papiers qu'il faut virer,   
c'est le racisme et la précarité   

Des familles à la rue, on n’en veut plus
Des papiers pour tous, des logements pour tous ! 
