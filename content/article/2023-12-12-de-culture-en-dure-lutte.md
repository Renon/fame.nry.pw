---
title: De culture en dure lutte
date: 2022-04-26T15:50:06.867Z
draft: false
tags:
  - stratégie
  - réflexions
  - occupation
---
# De culture en dure lutte

## Mise au poing n°7 - Avril 2021

Salut les hétérotopistes,\
\
En avril on crame le fil rouge par les deux bouts, on s'occupe des lieux de cuculture et ça déménage. Ainsi la FAME inaugurait-elle son Atelier National Populaire Enchantié le 17/04/21 dernier au TNP de Villeurbanne - pile cinq ans après ses premiers balbutiements aux Nuits Debouts en 2016. Mais peut-on réellement en rendre conte.

Répéter encore ?\
    .Des temps de travail musicaux et/ou réflexifs sont désormais proposés **chaque mardi à 18h au TNP**, en laissant la priorité au premier mardi de chaque mois pour des discussions, décisions, et répèt en tutti. On attachera une attention toute particulière à ce que la régularité de ces moments en groupe n'excluent pas pour autant celles et ceux qui n'y pourraient participer, par exemple en partageant au maximum ce qu'on veut y faire en amont et ce qu'on y a fait en aval.\
    .Là comme ailleurs, une inscription sur [l'agenda rénové](https://cloud.nry.pw/s/2FSkJin38A6YGW5 "https\://cloud.nry.pw/s/2FSkJin38A6YGW5") à favoriser immédiatement (mille mercis à sa bailleuse et à son tavernier, dans l'attente de compléter cette gratitude par d'autres moyens) facilitera grandement la logistique théorico-pratique des ces ateliers.\
    .La partie réflexive pourra prendre la forme de **dossiers à instruire** **au premier mardi** de chaque mois, préparés et portés par une ou deux personnes pour nourrir voire animer les débats occasionés autour des thématiques abordées. Exemples survolés ce 18/04 : quelle légitimité à s'approprier et incarner des chants écrits pour et par d'autres que nous ? quelle place dans quels rassemblements ~ musique festive et sujets graves ? quoi communiquer à qui, par quels moyens et par quels outils ? ...\
    .Une mise au point spécifique (choix des morceaux, des slogans, du placement, des modalités de dispsersion, etc.) est par ailleurs instituée **avant chaque action**, assortie si possible d'un débrief' apéritoire post-traumatique.\
    .Le désir de **multiplier les résidences** pour les moins disponibles (et non moins engagé⋅es) a également été exprimé.

Répéter en corps !\
+ Durant ces deux jours ont été montés/répétés/arrangés les tubes suivants (retravaillés en [répouet du 21/04](https://cloud.nry.pw/index.php/s/5E2Y2wfi3QRpFKd?path=%2FAudios%2Fr%C3%A9sidence%20TNP%2004.21 "https\://cloud.nry.pw/index.php/s/5E2Y2wfi3QRpFKd?path=%2FAudios%2Fr%C3%A9sidence%20TNP%2004.21")) :\
    .Danser encore (joué sur le parvis avec la chorale du TNP, structure au signe)\
    .L'hymen des femmes (les basses s'introduisent, on thème ensemble deux fois, couple 1, couple 2, reprise du thème par toustes).\
    .Colomentality (résurrection de la partie perdue des clarinettes)\
    .Kustino Oro (A, B, break puis C à l'appel des aigûs, solos sur le A tartiné de nappes à propos, slogans sur le B)\
    .Abaletapo Lissié\
    .Bella Ciao\
    .La semaine sanglande (à la sauce invisible)

+ rappel des signes\
    main sur la tête = reprise du thème\
    index levé = partie 1\
    index + majeur = partie 2\
    pouce + index + majeur = partie 3\
    petit doigt = reprise piano du 1 par les instrudiscrets\
    pouce + petit doigt = solo à accaparer\
    index + majeur en translation verticale descendante = diminuer le volume\
    poing fermé rotatif main droite = fin du morceau imminente\
    poing fermé fixe main gauche = Oui, mon cœur je le jette à la Révolution !\
\
+ rappel tactique\
    .ne pas s'essoufler en manif -> longues pauses entre les morceaux\
    .cohésion physique et musicale -> rester groupé, faire bloc\
    .la personne qui signe s'assure que tout le monde l'a vue\
    .une personne référente pourrait suivre l'articulation de la FAME à l'action pour anticiper d'éventuelles improvisations...

*Aperçu des thématiques abordées ces deux jours, à développer lors de prochains ébats.*

+ quelle légitimité a-t-on à s'approprier et incarner des chants écrits pour et par d'autres que nous ?\
    .Si le public est mal à l'aise, il a le choix de nous écouter (ou pas ?) -> prendre le temps de discuter avec les susconcerné⋅es\
    .Défricher l'histoire et la recontextualisation du morceau lors de ses premières répétitions\
    .Alternative paradigmatique entrevue pour l'Hymne des femmes :\
        - soit on arrête de le jouer\
        - soit on se met au service d'une chorale\
        - soit on se le réapproprie entièrement\
\
+ quelle place dans quels rassemblements ?\
    .Ne surtout pas hiérarchiser ni décrédibiliser les luttes\
    .Tisser des liens avant et pendant avec l'orga\
    .Construire éventuellement une prise de position consentie avec les présent⋅es avant les actions qui nous sollicitent.

+ quelle comm', avec quels moyens et outils ?\
    .Actualisation des supports matériels (affiches avec adresse web, mail, rdv TNP...)\
    .Choix des supports dématérialisés, actualisation du message d'accueil sur le mailing, multiplication et explicitation des référent⋅es⁴.\
    .Accompagnement des nouvelles têtes, **parrainage ?**\
    .Quel rapport à l'image photographiée et diffusée ?\
    .Qui porte la parole de la FAME, quand et comment ?\
    .Avant l'action, échanger quelques numéros de tél.\
    .Recrutement de bouches et d'oreilles.

Hauts les coeurs, à-bras-le-corps et qu'on s'y jette !

Pour une autre galaxie,\
Capitaine F.L.A.M.E.\
\
PS : Les news concernant la Commune Fanfare que préparent les Invisibles à Paris pour le 21-22-23 mai devraient nous être transmises pour le 2 mai.\
Les musicien⋅nes de la FAME réuni⋅es en apéro ce 17/04 décident à l'humanimité de bloquer cette date quoiqu'il en coûte, et d'y organiser au pire une action commune aux alentours de Lyon.\
\
¹ lesquelles concernent tous les rôles que peut compter la FAME (cf. [facteur d'autobus](https://fr.wikipedia.org/wiki/Facteur_d%27autobus "https\://fr.wikipedia.org/wiki/Facteur_d%27autobus"), [limites de l'horizontalisme](https://www.education-populaire.fr/la-tyrannie-de-labsence-de-structure/ "https\://www.education-populaire.fr/la-tyrannie-de-labsence-de-structure/"))