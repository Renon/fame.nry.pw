---
title: Appel à témoignages et à vidéos - violences policières sur fanfariste
date: 2024-06-25T21:19:29.840Z
draft: false
tags:
  - violences policières
---
**Vendredi 14 juin 202**4 à Lyon, un musicien de la fanfare FAME a été **frappé à la tête par un CRS** lors de la charge dans la **manifestation contre l’extrême droite** entre 22h et 22h15 qui a eut lieu près du 24 rue Mazenod dans le 3e. Il avait son instrument de musique à la main et a été blessé jusqu’au sang. Il est sorti de l'hôpital le lendemain matin avec **5 points de suture.** Nous lançons un **appel à témoignages et à vidéos,** n’hésitez pas [](<>)à partager et à nous écrire sur [famelyon@disroot.org](mailto:famelyon@disroot.org).

Merci de partager !