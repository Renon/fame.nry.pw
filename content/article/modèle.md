---
title: "Ici le titre de votre pamphlet"
date: 2018-11-14T15:25:02+01:00
draft: true
tags: [structure, appel, lutte, nouveau_tag]
--- 

Ceci est un exemple de post.

Pensez à bien modifier le champ title et le champ date ci-dessus.

Pour que l'article soit publié, il faut mettre "false" (sans guillemets) dans le champ draft ci-dessus.

La liste des tags existants est consultable sur https://fame.nry.pw/article/ . Il est également possible de mettre de nouveaux tags dans la liste, qui seront alors simplement créés.


Les posts sont formatés à l'aide du système markdown, une syntaxe qui permet de mettre en forme simplement un texte (https://github.com/adam-p/markdown-here/wiki/Markdown-Here-Cheatsheet).

# Exemple de titre principal provocateur

## Sous-titre tout aussi provocateur

Du texte avec des *emphases*, du **gras** et du ~~rayé~~.

Une liste très simple :

- caca
- pipi
- capitaliste

[Un lien qu'il est bien](https://degooglisons-internet.org/fr/)

Et le logo de cette magnifique syntaxe :

![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")