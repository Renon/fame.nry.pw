---
title: Quand la FAME s'entête
date: 2023-03-17T10:47:31.793Z
draft: false
tags:
  - outils
  - numérique
  - gafam
---
## Quand la FAME s'entête

## Mise au poing n°8 - Mars 2023

Salut la [canaille](https://www.youtube.com/watch?v=5nIDNGt1ESc "https\://www.youtube.com/watch?v=5nIDNGt1ESc"),

Au vu de l'émulation qui nous gagne, de nos jolies sorties en cortège de tête ou des quelques oies sauvages lâchées hier soir, il serait peut-être pertinent, dans une perspective d'inclusivité, de prendre un petit temps pour échanger sur ce qui se joue collectivement dans notre nouvelle... radicalité ?

On pourrait par exemple faire le point sur, quand la FAME joue :\
    *\- quels risques sont attachés à quelle exposition (à toute forme de violence) ?\
    - quelle responsabilité collective en découle ?\
    - comment anticiper, adapter voire accorder nos tactiques en situation ?\
    - comment veiller les un⋅es sur les autres lorsque la manif part en vrille ?\
    - plus généralement, comment cultiver le soin, l'attention, l'écoute (musicale mais pas que) tout en déambulant ?*

Pour partager ces questionnements qui incombent à notre "entêtement" dans les manifs du moment, mais aussi et surtout pour accueillir les nouvelles têtes, on propose de se retrouver **ce dimanche 19/03 au CEFEDEM entre 14h et 17h15**, avant de rallier ensemble et avec nos instrus [l'action directe à Opéra](https://rebellyon.info/Mobilisation-a-l-appel-de-l-AG-arts-et-24633 "https\://rebellyon.info/Mobilisation-a-l-appel-de-l-AG-arts-et-24633") prévue à **17h30 pétante** par la commission ad hoc de l'AG Arts et Culture.

Tendresses et Révolutions,\
et place au spectacle !\
Guy Déborde

PS : La mise à disposition d'un espace au CEFEDEM nous est assurée par un certain trompettiste à pancartes, qui trouvera ici toute la gratitude qui lui revient. Néanmoins, il s'agira pour lui éviter toute embrouille, d'arriver **entre 14h et 14h14 par la petite porte au 14 rue du Palais Grillé à Lyon 02, métro Cordeliers vers le Monop'**. Les retardataires pourront se signaler sur le groupe éponyme, afin qu'on leur ouvre à leur arrivée.\
PPS : Notre participation musicale à l'action directe de 17h30 a été encouragée par des vrai⋅es orga, qui précisent que nous ne courons aucun risque spécifique. Il s'agira seulement de faire corps, a priori dans la rue.\
PPPS : Des velléités d'organisation d'une résidence idoine devraient poindre incessamment.