---
title: Mondanités répétitives
date: 2020-09-29T15:56:44.867Z
draft: false
tags:
  - stratégie
  - alliances
---
# Mondanités répétitives

## Mise au poing n°6 - Septembre 2020

Ô Activistes du septième jour Bonjour,\
\
Cette encyclique automnale, à l'attention des brebis égarées dans leur vie de famille ou d'infâmie, n'a d'autre velléité que d'inscrire dans le marbre numérique notre messe du 24ème de septembre dernier, grâcieusement hébergée en la demeure de Sainte Anne du Planning Familial, louée soit-elle. La rencontre a réuni plus de 13 apôtres de la révolution sacrée, et s'est déroulée en deux parties distinctes : la première consacrée à l'exorcisation de nos confessions intimes, la seconde vouée au recueillement de la parole musicale.\
\
Première partie\
_ L'organisation des prochaines messes reste attachée au deuxième mardi (jour n°2) et quatrième jeudi (jour n°4) de chaque mois, n'en déplaise aux indisponibles ou indisposés chroniques. Soit pour la fin d'année : **13oct., 29oct., 10nov., 26nov., 8déc**. (Merci Marie pour cette précision). Le 31déc. reste ouvert à la discussion.

_ La question des **liens avec l'Eglise Communale de la Guillotière (ECG)** a été posée, sous les formes suivantes : Est-il souhaitable et envisageable de s'en rapprocher sérieusement, au-delà de la simple utilisation de leur chapelle ? Pourrions-nous y imaginer des activités d'évangélisation musicale (Greg ?) ? Y en a-t-il parmi nous qui voudrai(en)t se responsabiliser sur la prise de contact y afférente ? Tout cela dans la mesure où la tolérance municipale à leur égard semble s'être étendue au delà de l'an prochain. Par ailleurs, la sacristie de la Friche Totem (anciennement Lamartine) pourrait nous ouvrir ses portes dès janvier 2021.\
\
_ L'antenne lyonnaise du Comité de soutien aux Sans Papiers (CSP++) rappelle que notre participation le **samedi 3oct**. au rassemblement organisé pour l'acte 2 de la **marche des Sans Papiers** est fortement attendue. Ce sera Place Bellecour vers 14h. Plein de **slogans** sont sur le site !\
\
_ Une opportunité de soutien au [collectif Quicury](https://quicury.frama.site/ "https\://quicury.frama.site/") en lutte contre l'aménagement d'une vaste zone d'activité commerciale en plein sud beaujolais, se présente le **17 oct. dès 10h00 à l'occasion de leur récolte festive**, qui pourrait inaugurer des envies zadistes (détails en suivant le lien susprécisé).\
\
_ Un **appel à caisse-clairiste** se révèle critique en cette période de disette percussive pour la FAME. Ouvrons notre coeur au tempo à baguette.\
\
Deuxième Partie\
_El Pueblo Unido : on y ajoute désormais une **partie chantée** (paroles authentique), après le premier refrain.\
\
_Adekalom : La structure retenue est QuestionsCuivre, RéponseBois (x2), Chant, QuestionsCuivre, RéopnseBois (x2), BreakPercu, Passage en rythmique "binaire" (en fai 6/8), 2 tournes de percu, Thème (attention, une neuvième mesure sert d'appel au refrain), Refrain, Chant et "nappes" en même temps, Thème tutti, Refrain tutti.\
\
_ Montserrat : Les ténors ou trombones ou oeufoniums qui s'ennuient pourront superposer une mitraillette balkanisante au riff de basse.\
\
_ Batumambe : la résurrection des parties C et D est attendue pour la messe prochaine. Révisons les à cette fin.

\
Que la paix soit dans votre coeur, et la révolte dans votre esprit, et vice et versa, et caetera.

\
Saint Marcel, trompettiste du Jugement Dernier.