---
title: Appel à collaboration et communication
date: 2018-11-14T15:25:02+01:00
draft: false
tags:
  - structure
  - appel
---
Une vaste campagne de recrutement est en cours, afin notamment de monter des actions avec chanteurs, danseurs, comédiens.

Des visuels pour affiches, cartes de visites, autocollants sont en élaboration, n'hésitez pas à en proposer. Idem pour les slogans. Quelques annonces seront également diffusées sur des radios et média associatifs (radio canut, rebellyon, etc.).