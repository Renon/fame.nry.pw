---
title: " Faire bloc (de tous bois, cuivres et voix)"
date: 2023-07-17T22:30:44.714Z
draft: false
tags:
  - rencontres internationales
  - organisation
  - interzbeul
---
## Faire bloc (de tous bois, cuivres et voix)

## Mise au poing n°9 - Juillet 2023

Très chère Foule Ambulante de Musiques [Ecoterroristes](https://www.youtube.com/watch?v=TeW5tlALx_I "https\://www.youtube.com/watch?v=TeW5tlALx_I"),

En pleine crise de la Bastille et pour n'en rien lâcher, nous nous sommes assemblée le 9 juillet dernier dans le jardin de Toky (qu'il en soit ici largement remercié), afin de manifester notre désir partagé d'une toute première...\
**rencontre de fanfares militantes à Lyon, du 30/03 au 1/04/2024** ! Dans cette jouissive perspective, les lignes qui suivent se contentent d'en rapporter les premiers contours ébauchés.

La discussion, à douze voix, s'est joyeusement emmêlée de questions majeures :

\--- Quels lieux, territoires ? ---\
    *Un QG avec cuisine et espaces collectifs, bruit possible et/ou dortoirs ?\
        > Île Égalité, GrrndZero, CCO la soie, La Friche (totem ??), autre ?

\--- Quels contenus ? ---\
    * Une grosse manif', un gros concert (ou pas), un grassemblement, des ateliers de partage d'exp, des ateliers en partenariat...\
        > Pour les ateliers : échanges d'outils, d'habitudes *in situ*, de place dans le cortège, de place pour la musique, de légitimité tjs questionnée\
        > Partage de chants, de (modes de production de) slogans, de films (le docu TITUBANDA de B. Vey)

\--- Quelles causes ? ---\
    * Vu la sortie de trêve hivernale et le large spectre des concerné⋅es : convergence autour des enjeux de LOGEMENT

\--- Quel⋅le⋅s allié⋅e⋅s ? ---\
    * Canulars, CLL, syndicats, assoc' de quartier, DAL, JST, cheminot⋅es, ...\
    * Plutôt éviter les collectivités, vu la manière dont elles non-traitent la question du logement

\--- Quelles fanfares ? ---\
    * Pas d'invit pour le moment, mais on en parle de vive (avec les Invisibles dans le Larzac, la Grelutte chez Adèle Planchard, et d'autres ailleurs).

... et de quelques sujets transverses :

\- Quel entresoir : d'abord envie de rencontrer des homologues, mais aussi de rester accessible.\
- Quel rythme-énergie : du vendredi soir au lundi de pâques, il faut tenir = veiller à ne pas SE cramer dans des nuits interminables.\
- Quel public : vise-t-on le faire pour ou le faire avec ?\
- Quelle inclusion : des invitées, des publics, des mal-logé⋅es, etc. ?

La date et l'endroit d'un prochain apéro sur le sujet n'ont pas encore été défini, mais on fera passer l'info sur ce fil.\
Un groupe Signal dédié s'est également créé. Pour le rejoindre, autant privilégier une demande en mp.

Le pavé est dans la place.\
À nous en saisir,

<3\
Vakharm le Rouge