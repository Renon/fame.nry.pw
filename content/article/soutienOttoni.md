---
title: "Soutien à la fanfare Ottoni A Scoppio (Milan)"
date: 2019-11-14T12:00:00+01:00
draft: false
tags: [activisme, appel, lutte, OttoniAScoppio]
--- 

## Communiqué

*Le 7 décembre 2014, au cours d’une manifestation de rue se déroulant devant La Scala, à Milan, la fanfare Ottoni A Scoppio est intervenue musicalement, dans un climat de tension entretenu par les forces de l’ordre. Cette intervention a permis un apaisement, réduisant le niveau de violence, ce que nous saluons comme une belle intention démocratique. Toutefois, trois ans plus tard, des poursuites judiciaires sont engagées contre deux musiciens de la fanfare suspectés de « rébellion, et de résistance violente à la police ».*

Cette qualification est inique, alors même qu’Ottoni A Scoppio est connue et reconnue en Europe pour son engagement et ses interventions de solidarité et de fraternité n’utilisant que la force de la musique pour soutenir les plus démunis. Pourtant, le jugement aura lieu le 14 novembre, avec des risques de condamnation à des amendes voire à des peines d’emprisonnement pour deux musiciens.

Les fanfares de rue signataires du présent communiqué apportent leur soutien entier et total à Giancarlo et Roberto. Chaque musicien et musicienne d’Europe engagé·e dans ce mouvement qui croise l’engagement musical avec l’engagement social, se sent inculpé·e au même titre.

Réprimer Giancarlo, c’est menacer Claudia trompettiste à Rome, Peter, saxophoniste à Innsbruck, Valérie tromboniste à Paris...
Condamner Roberto, c’est intimider Louisa à Bologne, Jean-Claude à Lille...
Réprimer Ottoni A scoppio, c’est réprimer la musique populaire, la joie de vivre, la solidarité et la fraternité.
Eux c’est nous !

Car l’enjeu de cette procédure, c’est qu’elle se montre répressive, mais aussi dissuasive ; cinq ans après des actes présumés de violences musicales, elle vise à envoyer un signal, à faire planer un climat de peur et de répression à l’endroit des pratiques musicales adossées aux mouvements sociaux.
C’est surtout une menace, une intimidation une invitation à se replier sur soi, à dissocier la musique de l’engagement social et politique.

L’Europe des solidarités a fort à faire contre la montée de la xénophobie et des populismes européens.
Le soutien musical de nos réseaux est un catalyseur d’énergie.
La musique populaire ne cédera pas face à la pression policière et judiciaire.
Le pouvoir judiciaire peut réprimer les musicien·ne·s, il n’empêchera pas la musique.

**Relaxe pour Giancarlo et Roberto d’Ottoni A Scoppio.**

---
## SIGNATAIRES
la Fanfare Invisible (Paris / France), les Canards des Cropettes (Genève / Suisse), Fanfarra Clandestina (São Paulo / Brésil), La Fonc (Milan / Italie), Fiati Sprecati (Florence / Italie), Street Noise Orchestra (Innsbruck / Autriche), Second Line Social Aid and Pleasure Society Brass Band (Cambridge / États-Unis), Die BrazzBanditen (Leipzig / Allemagne), The Stroud Red Band (Londres / Angleterre), Charanga Ventolin (Gijon / Espagne), Fanfare A Manif Engagée (Lyon / France), Musician Action Group – MAG (San Francisco / États-Unis), Fanfare van de Eerste Liefdesnacht (Amsterdam / Pays-Bas), Titubanda (Rome / Italie), Rude Mechanical Orchestra (New York / États-Unis), HONK! Festival of Activist Street Bands » (Somerville / USA)
