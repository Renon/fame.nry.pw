---
title: "Communiqué des Papillons"
date: 2020-01-30T11:25:02+01:00
draft: false
tags: [papillons, appel, lutte]
--- 


Quand les libéraux auront fini de nous faire perdre notre temps à justifier du notre, de dicter nos agendas, de polluer aussi bien la planète que notre tête, de dresser les mers et les océans contre les populations, de préempter toutes particules jusqu’à l’air qu’on respire, d’assécher la moindre virgule et détourner le sens des mots, quand enfin les libéraux en auront fini du monde fini, on sera peut-être encore là (on est là, on est là), toujours persistants au-delà même de l'âge pivot ou de l’entendement.

Cependant, nous tenons à faire la mise au point suivante : il n’y aura pas de retour à la normale, pas plus que de retour à la croissance, pas même de retour à l’équilibre où nous reprendrions humblement notre place parmi les espèces et dans le concert des startup nations. Non, c’est tout à fait impossible. Car ce dont nous sommes sûrs c’est qu’entre-temps les libéraux auront été balayés dans le grand maelström du vivant tandis que nous, nous aurons muté. La promesse des chenilles n’engage pas les papillons.


 