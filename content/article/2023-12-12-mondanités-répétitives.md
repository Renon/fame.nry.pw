---
title: Actions et Diffusion
date: 2018-11-07T16:51:36.328Z
draft: false
tags:
  - bienvenue
---
# Actions & Diffusion

## Mise au poing n°4 - Novembre 2018

Camarades de tous pupitres et d'ailleurs, bonjour !

Ce courriel se veut un mensonge de bienvenue aux derniers arrivistes, en même temps qu'une mise à jour annuelle de nos vélleités d'organisation (exprimées en résidence le 21/10 dernier).

**0. Bienvenue**

*FAME est un virus théorique. FAME signifie l'abandon de soi aux évènements. Dites oui à une vie tendant résolument à sa propre négation.  FAME se refuse à être contemporain de toute cette merde. FAME réconcilie l'art et le quotidien afin qu'ils puissent avoir des rapports sexuels contre-nature dans des ruelles mal FAMées. FAME, c'est l'apatridie de l'esprit, l'ennemi de la masse et du parti. FAME vomit autant l'ordre stylé que le désordre stylisé propres à l'esthétique contemporaine.*

**1. Structure**

FAME est également un groupe de musiciens, né en 2016 durant le mouvement des Nuits Debouts, dans l'idée d'exploiter la musique comme force de soutien, voire de proposition, dans un cadre activiste et/ou militant. Son effectif total est pour l'instant estimé à 25 actifs, pour une moyenne de huit personnes par sortie. L'adhésion au groupe est entièrement ouverte, et s'exprime par l'inscription à cette liste de diffusion (*via* un mail à [sympa@lists.riseup.net](mailto:sympa@lists.riseup.net "mailto\:sympa@lists.riseup.net") avec pour objet subscribe fame-lyon). Aucune expertise musicale n'est évidemment requise, le groupe répéte une fois par mois, l'autonomie est vivement encouragée.

**2. Actions et initiatives**

FAME s'est construite sur de nombreuses participations à des manifestations de tous genres, à l'initiative de ses membres, voire sur invitations expresses, sans autre concertation préalable que quelques réponses par mail. Nous avons à coeur néanmoins de faire évoluer cette pratique, non seulement pour diversifier les causes et les modes d'actions, mais aussi pour enrichir nos positions respectives au travers d'échanges approfondis en répét' ou débrief' post-actions. À l'autre bout de l'échelle se trouve la Fanfare Invisible, qui passe 2h30 à débattre de sa position dans le cortège lors de ses répét' hebdomadaires.

Les luttes que nous soutenons se partagent principalement entre :\
- L'aide aux migrants (collectifs JST, RESF, ...)\
- Féminismes en actes (encore trop peu d'actionsFAME)\
- Droits LGBTQA+ (idem)\
- Sensibilisation et Résistance aux GAFAM (idem)\
- L'anti capitalisme face la casse du service publique (CNT, Sud, CGT, ...)\
- L'anti capitalisme face à la destruction des écosystèmes (Greenpeace, ZAD vs GPII, ...)\
- Les attaques du capitalisme financier (ATTAC, ...)\
- L'usage de la bicyclette, sortie de l'automobile (Alternatiba, Chatperché, ...)\
...

Lors du festival SBANDATA ROMANA organisé début juillet 2018 par la Titubanda (notre homologue romaine très expérimentée), sur le thème Musique et Activisme, une vingtaine de groupes/fanfares ont réfléchi pendant 6h de débat dominical à des formes d'actions synchronisées à l'échelle européenne. Des news prochaines à ce sujet, relancé par Nico S. en vue des permanences des élections euro.

**3. Communication**

3.1 Communication interne\
\
FAME dispose actuellement d'un [googledrive](https://drive.google.com/drive/folders/0BwY4HIVSGiM7MXBZUTZfd0JWSDg?usp=sharing "https\://drive.google.com/drive/folders/0BwY4HIVSGiM7MXBZUTZfd0JWSDg?usp=sharing") réunissant les morceaux, paroles et slogans divers, tandis que l'agenda commun se tient sur [ce framadate](https://framadate.org/lQSWuCrIFPdOYGnIrGEbevu2/admin "https\://framadate.org/lQSWuCrIFPdOYGnIrGEbevu2/admin") et les annonces ou discussions se déroulent sur cette liste (**une modération automatique oblige chacun de nous à confirmer l'envoi de nos mails en suivant la manip' expliquée après un premier envoi**). Ce fonctionnement désuet en regard des technologies group-friendly disponibles et du développement hautement invasif de Google, va disparaître grâce à une expertise de renon : un site est en préparation sur un serveur perso.

3.2 Communication externe

Une vaste campagne de recrutement est en cours, afin notamment de monter des actions avec chanteurs, danseurs, comédiens ?\
Des visuels pour affiches, cartes de visites, autocollants sont en élaboration, n'hésitez pas à en proposer. Idem pour les slogans. Quelques annonces seront également diffusées sur des radio et média associatifs (rcanut, rebellyon, etc.)

Amour&Révolte,

Bob, le Piranha Enivré.\
\
PS : À demain en répouét, 18h30 @LaFricheLamartine !