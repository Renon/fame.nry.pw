---
title: Répétitions et bavardages
date: 2019-03-26T16:58:17.985Z
draft: false
tags:
  - outils
  - numérique
  - gafam
---
# Répétitions et bavardages

## Mise au poing n°5 - Mars 2019

Bonjour les Fameux,

Vous voilà bien en hâte de savoir ce qui s'est dit lors de notre discussion de la semaine, dans les locaux frais et accueillants de la Friche, n'est ce pas ?

1. Visiblement, la Fame n'est pas un "projet de côté".

Les musiciens présents ont envie de répéter, et de répéter plus.

Et même de faire plus que répéter lors des répétitions...

Ca se passera systématiquement le 2e mardi et le 4e jeudi de chaque mois.

**Pour toujours plus de Fame dans ta vie,** **nous t'invitons à noter ces futures dates de repet, à chaque fois à 19h :**

\- mardi 9 avril

\- jeudi 25 avril

\- mardi 14 mai

\- jeudi 23 mai

\- mardi 11 juin

\- jeudi 27 juin

2. Ce pourra être dans la Friche dans un 1er temps, mais puisque à partir de mi-mai la Friche ne sera plus, nous répéterons ailleurs, et surement dehors.\
   \
3. Les répétitions commenceront à **19h par une heure de discussion, puis on joue.**

Elles sont ouvertes à tous musicien nouveau, aguerri, chanteur, danseur, curieux...

4. Plusieurs voix se sont élevées pour **augmenter le répertoire** de la FAME. Cher fameux, toi qui a un morceau sous le coude, mets le sur le [drive](https://cloud.nry.pw/index.php/s/5E2Y2wfi3QRpFKd?path=%2F "https\://cloud.nry.pw/index.php/s/5E2Y2wfi3QRpFKd?path=%2F"), et vient avec à la prochaine répet !
5. Il a été réaffirmé qu'en dessous d'une percu (de type grosse caisse ou couvercle de casseroles) + une basse (de type souba) + 1 médium + 1 aiguë (de type sax + trompette), la FAME n'était pas jouable, et qu'il valait mieux ne pas valider de plans à trop peu nombreux.
6. **La campagne de recrutement de nouveaux musiciens/chanteurs/danseurs...** va être relancée. Pour se faire :

\- Olivier nous prépare une 3e affiche pour 3 déclinaisons en tout avec autre chose que des hommes dessus. Si tu as dans tes archives une image de femme de type militante ou de type "chaque jour je nettoie mon cerveau avec la Fame", n'hésite pas à envoyer !

Ensuite on imprime, on diffuse et on envoie un mail large à tous les musiciens / fanfares de nos réseaux.

\- Les partitions seront bientôt  mises sur le [site de la Fame](https://fame.nry.pw/ "https\://fame.nry.pw/") pour être accessibles à n'importe qui et appâter le chaland.

\

7. Une liste de téléphone de fameux est en préparation et sera accessible avec un mot de passe sur notre Cloud. Flowrentin nous prépare ça : une fois prêt il te suffira de demander le mot de passe à ton voisin lors d'une repet ou manif à venir.
8. Flowrentin va former quelques administrateurs triés sur le volet à la gestion du site pour que les dates de plans et repet et autres infos puissent être ajoutées au fur et à mesure.
9. Il y a eu de nombreux autres discussion sur des thèmes comme "la FAME doit faire autre chose que manifester" "La FAME doit parler d'une seule voix VS la FAME n'est pas un espace de consensus" "la FAME doit travailler sa choré / ses slogans / son grain de peau"...

Ses débats sont grands ouverts et nous poursuivrons la discussion et les bonnes idées : durant le 1ere heure de chaque repet + 1h avant chaque plan (manif, rassemblement, action...)

10. A part ça, on a répété quelques morceaux, la prochaine fois on aimerait travailler entre autre **Merci patro**n, donc allez voir les partitions d'ici là :-)

Bisous, ciel bleu et sourcil levé,

Marie