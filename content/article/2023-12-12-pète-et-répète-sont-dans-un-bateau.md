---
title: Pète et répète sont dans un bateau
date: 2017-11-01T16:59:30.784Z
draft: false
tags:
  - recrutement
---
# Pète et répète sont dans un bateau...

## Mises au poing n°3 et n°2 - Novembre 2017

FAME, il est encore temps de donner un sens à ta vie.\
\
Pour cela, il suffit que toi, qui luit seul.e au dedans derrière ton écran, tu nous rejoignes en corps. Pour resplendir ensemble, au dehors et sans écran !\
\
Je te propose à cet effet l'utilisation régulière d'un [guichet unique](https://framacalc.org/PlansFameux "https\://framacalc.org/PlansFameux") (pour répondre ET pour proposer), combinée à une relecture jouissive de la mise au poing n°2 ci-dessous :\
\
"Amis famistes bonjour !\
\
Voici un condensé de ce qui s'est dit l'autre soir (19/10/16) chez Hervé. Pour le détail, voir en PJ. L'idée était d'égrener les résultats du sondage (19 réponses), pour tenter d'accorder une ébauche d'organisation avec les désirs et disponibilités de chacun. Les points sont des lilas :

1. FAME est **hétérogène**, et très érogène, à tout égard (niveau, implication, objectifs, moyens, idéolo).
2. FAME est une **boule de neige**. Sa masse critique pour rouler a été arrêtée à : au moins une basse+percu, et au moins 5 sur les "gros" rassemblements-manifs. Et nécessité d'une bonne **dynamique de boule** (sur le long terme) !
3. FAME est idéologiquement in**dépendante**, mais son impact dépend de son environnement proche -> S'adapter à cet espace (ie, circuler si ça ne prends pas) vs. Préparer la performance pour que ça prenne partout. Equilibre à décider en amont des manifs !
4. FAME veut **répéter une fois par mois**. Contenu : Musical ET Brainstorm ET Occupation de l'espace contestataire. Lieux : Friche Lamartine ? Chez nous (6 ou 7 max) ?  Précisions, doodle, etc. à venir.
5. FAME souhaiterait **un crieur et son mégaphone**. Mais dès demain, préparer des slogans, les échanger par mail au moins 1h avant les évenements.
6. FAME exige **plus d'organisation**. Désormais, une manif = DEUX mails : le premier avec détails et doodle, le dernier 6h avant au plus tard pour confirmer ou non la présence de FAME en fonction du remplissage.

Au plaisir de nous croiser prochainement.\
O/