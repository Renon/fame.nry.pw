// fames contient la liste des divers explications à l'acronyme FAME
// n'hésitez pas à en rajouter, en copiant une ligne 
// il doit toujours y avoir 4 groupes de mots par explication

var fames = [
  ["Fanfare","À","Manif","Engagée"],
  ["Festoyeurs","Avinés contre","Machines","Écervelées"],
  ["Fanfare","Activiste pour","Mirlitons","Entêtés"],
  ["Fanfare","d'Action","Militante et","Enjouée"],
  ["Festin","Aléatoire de","Musiciens","Énervés"],
  ["Fainéants","Assermentés à","Mécaniques","Éculées"],
  ["Fêtards","Anarchistes d'un","Monde","Ensoleillé"],
  ["Fabrique","Acrobatique de","Moments","Éclairés"],
  ["Factieux","Alarmistes d'un","Modernisme","Embourbé"],
  ["Fidèles","Adorateurs des","Mécanismes","Économiques"],
  ["Farce","Abrutissante en","Militantisme","Érigée"],
  ["Fanfare","À","Manif","Engagée"],
  ["Fanfare","À","Manif","Engagée"],
  ["Flou","Artistique et","Modèle","Éclectique"],
  ];
